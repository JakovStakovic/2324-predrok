from django.urls import path, include
from django.contrib import admin
from . import views



urlpatterns = [
    path('', views.home, name = "home"),
    path('contact/', views.contact, name="contact"),
    path('bowling/', views.bowling, name="bowling"),
    path('billiards/', views.billiards, name="billiards"),
    path('darts/', views.darts, name="darts"),
    path('bowling/price/', views.bowlingPrice, name="bowlingPrice"),
    path('billiards/price/', views.billiardsPrice, name="billiardsPrice"),
    path('darts/price/', views.dartsPrice, name="dartsPrice"),
    path('accounts/', include('accounts.urls')),
    path('booking/', views.booking, name='booking'),
    path('booking-submit/', views.bookingSubmit, name='bookingSubmit'),
    path('user-panel', views.userPanel, name='userPanel'),
    path('user-update/<int:id>', views.userUpdate, name='userUpdate'),
    path('user-update-submit/<int:id>', views.userUpdateSubmit, name='userUpdateSubmit'),
    path('staff-panel/', views.staffPanel, name='staffPanel'),
    path('staffDelete/<int:id>', views.staffDelete, name='staffDelete'),
    path('userCancel/<int:id>', views.userCancel, name='userCancel'),
    path('merchshop/', include('merchshop.urls')),
]