from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.urls import reverse
from django.views.decorators.http import require_POST
from django.core.mail import send_mail
from .models import *
from django.contrib import messages
from datetime import datetime, timedelta


# Create your views here.
def home(request):
    context = []
    return render(request, 'base/home.html')

def contact(request):
    context= []
    return render(request, 'base/contact.html')

def bowling(request):
    return render(request, 'base/bowling.html')

def billiards(request):
    return render(request, 'base/billiards.html')

def darts(request):
    return render(request, 'base/darts.html')

def bowlingPrice(request):
    return render(request, 'base/bowlingPrice.html')

def billiardsPrice(request):
    return render(request, 'base/billiardsPrice.html')

def dartsPrice(request):
    return render(request, 'base/dartsPrice.html')


def contact(request):
    if(request.method == 'POST'):
        name = request.POST.get('name')
        email = request.POST.get('email')
        message = request.POST.get('message')

        recipient = 'arcadeBarhr@gmail.com'  

        send_mail(
            f'New message from {name}',
            f'From: {email}\n\n{message}',
            email,
            [recipient],
            fail_silently=False,
        )

        messages.success(request, 'Your message has been sent successfully!')
        return redirect('home')
    else:
        return render(request, 'base/contact.html')


def booking(request):
    #Calling 'validWeekday' Function to Loop days you want in the next 21 days:
    weekdays = validWeekday(22)

    #Only show the days that are not full:
    validateWeekdays = isWeekdayValid(weekdays)
    

    if request.method == 'POST':
        service = request.POST.get('service')
        day = request.POST.get('day')
        description = request.POST.get('description')
        if service == None:
            messages.success(request, "Please Select A Service!")
            return redirect('booking')

        #Store day and service in django session:
        request.session['day'] = day
        request.session['service'] = service
        request.session['description'] = description

        return redirect('bookingSubmit')

    return render(request, 'base/booking.html', {
        'weekdays':weekdays,
        'validateWeekdays':validateWeekdays,
    })


def bookingSubmit(request):
    user = request.user
    times = [
        "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM"
    ]
    today = datetime.now()
    minDate = today.strftime('%Y-%m-%d')
    deltatime = today + timedelta(days=21)
    strdeltatime = deltatime.strftime('%Y-%m-%d')
    maxDate = strdeltatime

    #Get stored data from django session:
    day = request.session.get('day')
    service = request.session.get('service')
    #Only show the time of the day that has not been selected before:
    hour = checkTime(times, day)
    if request.method == 'POST':
        time = request.POST.get("time")
        date = dayToWeekday(day)

        if service != None:
            if day <= maxDate and day >= minDate:
                if date == 'Sunday' or date == 'Saturday' or date == 'Wednesday' or date == 'Thursday' or date == 'Friday' or date == 'Tuesday': 
                    if Appointment.objects.filter(day=day).count() < 11:
                        if Appointment.objects.filter(day=day, time=time).count() < 1:
                            AppointmentForm = Appointment.objects.get_or_create(
                                user = user,
                                service = service,
                                day = day,
                                time = time,
                                
                            )
                            messages.success(request, "Appointment Saved!")
                            return redirect('userPanel')
                        else:
                            messages.success(request, "The Selected Time Has Been Reserved Before!")
                    else:
                        messages.success(request, "The Selected Day Is Full!")
                else:
                    messages.success(request, "The Selected Date Is Incorrect")
            else:
                    messages.success(request, "The Selected Date Isn't In The Correct Time Period!")
        else:
            messages.success(request, "Please Select A Service!")


    return render(request, 'base/bookingSubmit.html', {
        'times':hour,
    })

def userPanel(request):
    user = request.user
    appointments = Appointment.objects.filter(user=user).order_by('day', 'time')
    return render(request, 'base/userPanel.html', {
        'user':user,
        'appointments':appointments,
    })

def userUpdate(request, id):
    appointment = Appointment.objects.get(pk=id)
    userdatepicked = appointment.day
    #Copy  booking:
    today = datetime.today()
    minDate = today.strftime('%Y-%m-%d')

    #24h if statement in template:
    delta24 = (userdatepicked).strftime('%Y-%m-%d') >= (today + timedelta(days=1)).strftime('%Y-%m-%d')
    #Calling 'validWeekday' Function to Loop days you want in the next 21 days:
    weekdays = validWeekday(22)

    #Only show the days that are not full:
    validateWeekdays = isWeekdayValid(weekdays)
    

    if request.method == 'POST':
        service = request.POST.get('service')
        day = request.POST.get('day')

        #Store day and service in django session:
        request.session['day'] = day
        request.session['service'] = service

        return redirect('userUpdateSubmit', id=id)


    return render(request, 'base/userUpdate.html', {
            'weekdays':weekdays,
            'validateWeekdays':validateWeekdays,
            'delta24': delta24,
            'id': id,
        })

def userUpdateSubmit(request, id):
    user = request.user
    times = [
        "3 PM", "4 PM", "5 PM", "6 PM", "7 PM", "8 PM", "9 PM", "10 PM"
    ]
    today = datetime.now()
    minDate = today.strftime('%Y-%m-%d')
    deltatime = today + timedelta(days=21)
    strdeltatime = deltatime.strftime('%Y-%m-%d')
    maxDate = strdeltatime

    day = request.session.get('day')
    service = request.session.get('service')
    
    #Only show the time of the day that has not been selected before and the time he is editing:
    hour = checkEditTime(times, day, id)
    appointment = Appointment.objects.get(pk=id)
    userSelectedTime = appointment.time
    if request.method == 'POST':
        time = request.POST.get("time")
        date = dayToWeekday(day)

        if service != None:
            if day <= maxDate and day >= minDate:
                if date == 'Sunday' or date == 'Tuesday' or date == 'Thursday' or date == 'Wednesday' or date == 'Friday' or date == 'Saturday':
                    if Appointment.objects.filter(day=day).count() < 11:
                        if Appointment.objects.filter(day=day, time=time).count() < 1 or userSelectedTime == time:
                            AppointmentForm = Appointment.objects.filter(pk=id).update(
                                user = user,
                                service = service,
                                day = day,
                                time = time
                            ) 
                            messages.success(request, "Appointment Edited!")
                            return redirect('userPanel')
                        else:
                            messages.success(request, "The Selected Time Has Been Reserved Before!")
                    else:
                        messages.success(request, "The Selected Day Is Full!")
                else:
                    messages.success(request, "The Selected Date Is Incorrect")
            else:
                    messages.success(request, "The Selected Date Isn't In The Correct Time Period!")
        else:
            messages.success(request, "Please Select A Service!")
        return redirect('userPanel')


    return render(request, 'base/userUpdateSubmit.html', {
        'times':hour,
        'id': id,
    })


def dayToWeekday(x):
    z = datetime.strptime(x, "%Y-%m-%d")
    y = z.strftime('%A')
    return y

def validWeekday(days):
    #Loop days you want in the next 21 days:
    today = datetime.now()
    weekdays = []
    for i in range (0, days):
        x = today + timedelta(days=i)
        y = x.strftime('%A')
        if y == 'Sunday' or y == 'Tuesday' or y == 'Wednesday' or y == 'Thursday' or y == 'Friday' or y == 'Saturday':
            weekdays.append(x.strftime('%Y-%m-%d'))
    return weekdays
    
def isWeekdayValid(x):
    validateWeekdays = []
    for j in x:
        if Appointment.objects.filter(day=j).count() < 10:
            validateWeekdays.append(j)
    return validateWeekdays

def checkTime(times, day):
    #Only show the time of the day that has not been selected before:
    x = []
    for k in times:
        if Appointment.objects.filter(day=day, time=k).count() < 1:
            x.append(k)
    return x

def checkEditTime(times, day, id):
    #Only show the time of the day that has not been selected before:
    x = []
    appointment = Appointment.objects.get(pk=id)
    time = appointment.time
    for k in times:
        if Appointment.objects.filter(day=day, time=k).count() < 1 or time == k:
            x.append(k)
    return x



def staffPanel(request):
    appointments = Appointment.objects.all()
    context = {
        'appointments' : appointments
    }
    return render(request, 'base/staffPanel.html', context)


def staffDelete(request, id):
    appointment = Appointment.objects.get(pk=id)
    appointment.delete()
    messages.success(request, "Appointment successfully deleted by staff: " + request.user.username + "!")
    return redirect('staffPanel')

def userCancel(request, id):
    appointment = Appointment.objects.get(pk=id)
    created_time = appointment.time_ordered
    created_time = created_time.replace(tzinfo=None)
    current_time = datetime.now().replace(tzinfo=None)
    time_difference = current_time - created_time
    if(request.user.is_staff == False):
        if(time_difference < timedelta(minutes=16)):
            appointment.delete()
            messages.success(request, "Appointment successfully cancelled!")
        else:
            messages.success(request, "Appointment cannot be cancelled after 15 minutes!")
    else:
        appointment.delete()
        messages.success(request,"Appointment successfully cancelled by staff: " + request.user.username + "!")
    return redirect('userPanel')