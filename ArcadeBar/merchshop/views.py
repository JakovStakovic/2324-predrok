from pyexpat.errors import messages
import re
from django.shortcuts import render
from . models import Article, Order
from accounts.models import CustomUser
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
# Create your views here.


def merch(request):
    articles = Article.objects.all()
    context = {'articles': articles}
    return render(request, 'merchshop/merch.html', context)



def cart(request, id):
    if request.method == 'POST':
        article = Article.objects.get(id=id)
        context = {'article': article}
        return render(request, 'merchshop/cart.html', context)
    else:
        return render(request, 'merchshop/cart.html')
    
def checkout(request, id):
    if request.method == 'POST':
        user = request.user
        user.first_name = request.POST.get('firstname')
        user.last_name = request.POST.get('lastname')
        user.address = request.POST.get('address')
        request.session['amount'] = request.POST.get('amount')
        amount1 = request.POST['amount']
        request.session['article'] = id
        request.session['firstname'] = request.POST.get('firstname')
        request.session['lastname'] = request.POST.get('lastname')
        request.session['address'] = request.POST.get('address')
        
        article = Article.objects.get(id=id)
        fullPrice = article.price * float(amount1)
        request.session['fullPrice'] = fullPrice 
        context = {'fullPrice': fullPrice}
        return render(request, 'merchshop/checkout.html', context)
    else:
        return render(request, 'base/base.html')
    

    
def thankYou(request, id):
    if request.method == 'POST':
        user = get_object_or_404(CustomUser, id=id)
        if(request.POST.get('cvc') == '000'):
            firstname = request.session['firstname']
            lastname = request.session['lastname']
            address = request.session['address']
            amount = request.session['amount']
            fullPrice = request.session['fullPrice']
            user.first_name = firstname
            user.last_name = lastname
            user.address = address
            user.save()
            context = {
                'fullPrice': fullPrice,
                'firstname': firstname,
                'lastname': lastname,
                'address': address,
                'amount': amount
                }
            return render(request, 'merchshop/thankYou.html', context) 
        else:
            error_message = "Wrong CVC"
            return render(request, 'merchshop/merch.html', {'error_message': error_message})
    else:
        return render(request, 'base/home.html')