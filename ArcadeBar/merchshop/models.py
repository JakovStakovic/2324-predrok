from django.db import models

# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=100)
    price = models.FloatField()
    image = models.URLField()
    description = models.TextField()


    def __str__ (self):
        return self.title + " - " + str(self.price)

class Order(models.Model):
    article = models.OneToOneField(Article, on_delete=models.CASCADE)
    amount = models.IntegerField()