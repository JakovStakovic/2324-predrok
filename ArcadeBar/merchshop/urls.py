from django.urls import path
from . import views

app_name = 'merchshop'
urlpatterns = [
    path('', views.merch, name='merch'),
    path('cart/<int:id>', views.cart, name="cart"),
    path('checkout/<int:id>', views.checkout, name="checkout"),
    path('thankYou/<int:id>', views.thankYou, name="thankYou"),
]