from django.db import models
from django.contrib.auth.models import AbstractUser
from django import forms
# Create your models here.
class CustomUser(AbstractUser):
    username = models.CharField(max_length=30, unique=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=254, unique=True)
    number = models.IntegerField(default=0, blank=True)
    is_staff = models.BooleanField(blank=True, default=False)
    address = models.TextField(blank=True, null=True)


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)
